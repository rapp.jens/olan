# OLAN - OLed-ANimator
OLAN is a simple animation program designed for use on SSD1306 or SH1106
displays. It creates a simple json based file format which can be transferred to
microcontrollers which will then display the animation.

The software is based on Python 3 and uses Tkinter as ui framework which should
be available by default so nothing else has to be installed to use OLAN.

Base Capabilities are:
- drawing currently enables to draw
    - dots or free-styled
    - straight lines
    - filled or blank rectangles
    - filled or blank ellipses
- lines can be aligend in 45 degree steps using the CTRL key
- rectangles and ellipses can be made squared or circles using the CTRL key
- Animation frames can be
    - added
    - copied
    - removed
- An animation frame shows the contents of the latter or next frame to make it
easier to create fluent animations
- helping lines can be drawn to create a grid for orientation
- orientation grids can be saved and loaded.

Still, there are many things to do:
- We have an infunctional run button
- Copy and paste of segments would be nice

## Development
This project has been developed by TecDroiD (rapp.jens@gmail.com)

## License
You should have received a license file with this project.

# Installation
Currently, pip is not set up so you will have to copy the project somewhere and
run oled_animator (maybe you will need to explicitely run it with python3)

# Usage
A manual [can be found here](doc/README.md).

# Fun
Have fun folks!

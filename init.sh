#!/bin/bash
##############################################################################
# author       # TecDroiD
# date         # 2023-08-02
# ---------------------------------------------------------------------------
# description  #
#              #
#              #
##############################################################################

if [[ ! -d env ]]; then
    echo "env does not exist, creating virtual environment"
    python -m venv env
fi

echo "activating environment..."
source env/bin/activate

echo "loading requirements..."
python -m pip install -r requirements.txt

echo "done."
deactivate


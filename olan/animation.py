##############################################################################
# author       # TecDroiD
# date         # 2023-08-14
# ---------------------------------------------------------------------------
# description  #
#              #
#              #
##############################################################################
import tkinter as tk

import json
import base64


class Animation():
    """ this is an animation for olan device
    """

    def __init__(self, data=[], resolution=[128, 64]):
        """ initialize with data or base data
        """
        if data is not None:
            self.frames = data
        else:
            self.frames = []

        self.resolution = resolution
        self.position = tk.IntVar()

        if self.num_frames == 0:
            self.add_frame()

    def clear(self):
        """ clear the system
        """
        self.frames.clear()
        self.add_frame()

    def insert_frame(self, data=None):
        """ insert a frame at current position
        """
        self.add_frame(data, self.position.get() + 1)

    def add_frame(self, data=None, index=None):
        """ add a frame
        """
        num_bytes = int(self.bits_per_image / 8)
        insert_at = self.num_frames if index is None else index

        if data is None:
            fb = bytearray(num_bytes)
        else:
            fb = data.copy()

        self.frames.insert(insert_at, fb)
        self.set(insert_at)
        return insert_at

    def to_file(self, filename):
        """ save me
        """
        data = {
            "resolution": self.resolution,
            "frames": []
            }

        for frame in self.frames:
            data["frames"].append(base64.b64encode(frame).decode("utf-8"))

        with open(filename, "w") as fp:
            json.dump(data, fp, indent=2)

    @staticmethod
    def from_file(filename):
        """ get one
        """
        with open(filename) as fp:
            js = json.load(fp)
            frames = []
            for frame in js["frames"]:
                frames.append(bytearray(base64.b64decode(frame)))

            return Animation(data=frames, resolution=js["resolution"])

    @property
    def bits_per_image(self):
        """ return the bits per image
        """
        res = self.resolution
        return res[0] * res[1]

    @property
    def num_frames(self):
        """ return the number of frames
        """
        return len(self.frames)

    @property
    def current_frame(self):
        """ get the current frame
        """
        return self.frames[self.position.get()]

    def remove(self, frame):
        """ remove the current frame
        """
        if frame in self.frames:
            self.frames.remove(frame)

            if self.position.get() >= len(self.frames):
                self.position.set(len(self.frames) - 1)

            if len(self.frames) == 0:
                self.add_frame()
                self.position.set(0)

    def set(self, number):
        """ set current frame
        """
        if number < 0:
            self.position.set(0)
        elif number < self.num_frames:
            self.position.set(number)

    def draw(self, canvas, color="black", scale=4):
        """ draw yourself to a canvas
        """
        for bnum, bval in enumerate(self.current_frame):
            for bit in range(7, 0, -1):
                bitval = (bval >> bit) % 1
                bitpos = bnum * 8 + bit
                if bitval == 1:
                    y = int(bitpos / self.resolution[0]) * scale
                    x = int(bitpos % self.resolution[0]) * scale
                    self.canvas.create_rectangle(
                        x, y,
                        x + scale, y + scale,
                        fill=color
                        )

    def setpix(self, x, y, value=1):
        """ set a pixel
        """
        pos = y * self.resolution[0] + x
        byte = int(pos / 8)
        bit = 7 - x % 8
        if value:
            # set value
            self.current_frame[byte] = self.current_frame[byte] | (1 << bit)
        else:
            # reset value
            self.current_frame[byte] = self.current_frame[byte] & ~(1 << bit)

    def getpix(self, x, y, frame=0):
        """ get a pixel
        """
        number = self.position.get() + frame
        if number < 0 or number >= len(self.frames):
            return 0
        frame = self.frames[number]
        pos = y * self.resolution[0] + x
        byte = int(pos / 8)
        bit = 7 - x % 8
        return (frame[byte] >> bit) & 1

##############################################################################
# author       # TecDroiD
# date         # 2023-08-24
# ---------------------------------------------------------------------------
# description  #
#              #
#              #
##############################################################################
import tkinter as tk
from tkinter import filedialog as fd

from .animation import Animation


class AnimationCanvas(tk.Canvas):
    """ this is a canvas that holds an animation
    """
    ANI_FILETYPES = [
        ("Animation-Files", "*.ani"),
        ("JSON-Files", "*.json"),
        ("All Files", "*.*"),
        ]

    def __init__(self, root, **kwargs):
        resolution = kwargs.pop("resolution", (128, 64))
        self.scale = kwargs.pop("scale", 4)
        self.posx = tk.IntVar(root)
        self.posy = tk.IntVar(root)

        # these are support lines
        self.grid = []

        # these are the history functions for undo and redo
        self.undostack = []
        self.redostack = []

        self._width = resolution[0] * self.scale
        self._height = resolution[1] * self.scale
        super().__init__(
            root,
            width=self._width,
            height=self._height,
            **kwargs
            )

        self.update_listeners = []
        self.data = Animation(resolution=resolution)
        self.new_file()

    def moved(self, event):
        """ just because you want to know where it's currently
        """
        self.posx.set(int(event.x / self.scale))
        self.posy.set(int(event.y / self.scale))

        self.moveto(self.vline, 0, event.y)
        self.moveto(self.hline, event.x, 0)

    def new_file(self, *args):
        """ create a new file
        """
        self.filename = None
        self.data.clear()
        self.clear()

    def load_file(self, *args):
        """ load file
        """
        self.filename = fd.askopenfilename(
            filetypes=self.ANI_FILETYPES,
            defaultextension=self.ANI_FILETYPES
            )
        self.data = Animation.from_file(self.filename)
        self.update()

    def save_file(self, *args):
        """ save the file
        """
        if self.filename is None:
            self.save_file_as()
        else:
            self.data.to_file(self.filename)

    def save_file_as(self, *args):
        """ save file as
        """
        self.filename = fd.asksaveasfilename(
            confirmoverwrite=True,
            filetypes=self.ANI_FILETYPES,
            defaultextension=self.ANI_FILETYPES
            )
        self.data.to_file(self.filename)

    def remove_current(self):
        """ remove the current frame
        """
        self.data.remove(self.data.current_frame)
        self.update()

    def copy_current(self):
        """ copy current frame to another one
        """
        self.data.add_frame(
            self.data.current_frame,
            self.data.position.get() + 1
            )
        self.update()

    def clear(self):
        """ just clean the canvase
        """

        self.delete("all")
        self.vline = self.create_line(
            0, 0,
            self._width, 0,
            fill="lightgreen",
            width=1
            )
        self.hline = self.create_line(
            0, 0,
            0, self._height,
            fill="lightgreen",
            width=1
            )
        for support in self.grid:
            self._draw_gridline(*support)

    def setpixel(self, x, y, val, colors=["white", "black"]):
        """ set a pixel on canvas and data
        """
        if x >= 0 and x < self.resolution[0] and \
                y >= 0 and y < self.resolution[1]:
            self._draw_pixel(x, y, val, colors)
            self.data.setpix(x, y, val)

    def _draw_pixel(self, x, y, val, colors=["white", "black"], inset=0):
        """ draw a pixel on the canvas
        """
        px = self.scale * x + inset
        py = self.scale * y + inset
        if colors[val] is not None:
            self.create_rectangle(
                px,
                py,
                px + self.scale - 2 * inset,
                py + self.scale - 2 * inset,
                fill=colors[val],
                outline=colors[val]
                )

    def create_gridline(self, x1, y1, x2, y2):
        """ create a support line
        """
        self.grid.append((x1, y1, x2, y2))
        self._draw_gridline(x1, y1, x2, y2)

    def _draw_gridline(self, x1, y1, x2, y2):
        """ draw a support line
        """
        hs = int(self.scale /2)
        self.create_line(
            x1 * self.scale+hs, y1 * self.scale+hs,
            x2 * self.scale+hs, y2 * self.scale+hs,
            fill="#8080ff",
            width=1
            )

    def add_callback(self, listener):
        """ add an update listener
        """
        self.update_listeners.append(listener)

    def update(self):
        """ update from current data frame
        """
        self.clear()
        for y in range(self.resolution[1]):
            for x in range(self.resolution[0]):
                self._draw_pixel(
                    x, y,
                    self.data.getpix(x, y, -1),
                    [None, "#ffe0e0"]
                    )
                self._draw_pixel(
                    x, y,
                    self.data.getpix(x, y, +1),
                    [None, "#e0e0ff"]
                    )
                self._draw_pixel(
                    x, y,
                    self.data.getpix(x, y),
                    [None, "black"]
                    )

        for callback in self.update_listeners:
            callback()

    def calc_pos(self, x, y):
        """ calculates a display position from mouse coordinates
        """
        return (int(x / self.scale), int(y / self.scale))

    def snapshot(self, action="draw"):
        """ create a snapshot and return it
        """
        return (
            self.data.position.get(),
            self.data.current_frame.copy(),
            action
            )

    def done(self, snapshot):
        """ the snapshot is done now
        """
        self.undostack.append(snapshot)
        self.redostack.clear()

    def undo(self, event):
        """ undo the last done thing
        """
        if len(self.undostack) > 0:
            oldsnap = self.undostack.pop(-1)
            newsnap = self.snapshot(oldsnap[2])
            self.data.frames[oldsnap[0]] = oldsnap[1].copy()
            self.redostack.append(newsnap)
            self.update()

    def redo(self, event):
        """ redo the last undone thing
        """
        if len(self.redostack) > 0:
            oldsnap = self.redostack.pop(-1)
            newsnap = self.snapshot(oldsnap[2])
            self.data.frames[oldsnap[0]] = oldsnap[1].copy()
            self.undostack.append(newsnap)
            self.update()

    @property
    def resolution(self):
        """ return the resolution of the image
        """
        return self.data.resolution

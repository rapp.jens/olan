#!/usr/bin/env python
##############################################################################
# author       # TecDroiD
# date         # 2023-08-02
# ---------------------------------------------------------------------------
# description  #
#              #
#              #
##############################################################################
import sys
import json

import tkinter as tk
from tkinter import ttk
from tkinter import filedialog as fd

from . import __version__
from .widgets import ImageButton
from .toolbox import ToolBox
from .animationcanvas import AnimationCanvas


class ImageNavigator(tk.Frame):
    def __init__(self, root, canvas, **kwargs):
        """ initialize
        """
        super().__init__(root, relief=tk.SUNKEN, **kwargs)
        self.canvas = canvas
        self.canvas.add_callback(self.update)

        ImageButton(
            self,
            "grfx/first.png",
            command=self.first
            ).pack(side=tk.LEFT)
        ImageButton(
            self,
            "grfx/previous.png",
            command=self.previous
            ).pack(side=tk.LEFT)
        self.pos = ttk.Combobox(
            self,
            textvariable=self.animation.position,
            width=4,
            )
        self.pos.pack(side=tk.LEFT)
        ImageButton(
            self,
            "grfx/next.png",
            command=self.next
            ).pack(side=tk.LEFT)
        ImageButton(
            self,
            "grfx/last.png",
            command=self.last
            ).pack(side=tk.LEFT)
        self.runstop = ImageButton(
            self,
            "grfx/run.png",
            # command=None
            ).pack(side=tk.LEFT)
        ImageButton(
            self,
            "grfx/add.png",
            command=self.add
            ).pack(side=tk.LEFT)
        ImageButton(
            self,
            "grfx/remove.png",
            command=self.remove
            ).pack(side=tk.LEFT)
        ImageButton(
            self,
            "grfx/copy_frame.png",
            command=self.copy
            ).pack(side=tk.LEFT)

        self.pxl = tk.Label(
            self,
            textvariable=self.canvas.posx
            ).pack(side=tk.RIGHT)
        self.pyl = tk.Label(
            self,
            textvariable=self.canvas.posy
            ).pack(side=tk.RIGHT)

        self.update()

    def remove(self):
        """ remove the current frame
        """
        self.canvas.remove_current()

    def copy(self):
        """ create a copy of the current element
        """
        self.canvas.copy_current()

    def update(self, *args):
        """ update your data
        """
        self.pos['values'] = [*range(self.animation.num_frames)]
        self.pos['textvariable'] = self.animation.position

    def previous(self):
        """ set previous image
        """
        self.animation.set(self.position - 1)
        self.canvas.update()

    def next(self):
        """ set next image
        """
        self.animation.set(self.position + 1)
        self.canvas.update()

    def first(self):
        """ set first element
        """
        self.animation.set(0)
        self.canvas.update()

    def last(self):
        """ set last element
        """
        self.animation.set(self.animation.num_frames - 1)
        self.canvas.update()

    def add(self):
        """ add a frame
        """
        self.animation.insert_frame()
        self.canvas.update()

    @property
    def position(self):
        """ get current position
        """
        return self.animation.position.get()

    @property
    def animation(self):
        """ get current animation
        """
        return self.canvas.data


class MainWindow():
    """ this is the application main window
    """
    BGCOLOR = "white"
    FGCOLOR = "black"
    PRVCOLOR = "gray"

    GRID_FILETYPES = [
        ("Grid-Files", "*.grid"),
        ("JSON-Files", "*.json"),
        ("All Files", "*.*"),
        ]

    def __init__(self, **kwargs):
        """ initialize anything
        """
        self.root = tk.Tk()
        self.root.title(f"OLed ANimator v{__version__}")

        frame = tk.Frame(self.root)
        self.canvas = AnimationCanvas(
            frame,
            background=MainWindow.BGCOLOR)

        self.canvas.pack(side=tk.TOP)
        self.navigator = ImageNavigator(frame, self.canvas).pack()
        self.toolbox = ToolBox(self.root, self.canvas).pack(side=tk.LEFT)

        self.init_menu()

        frame.pack(side=tk.RIGHT, fill=tk.Y)

    def init_menu(self):
        """ create the menu
        """
        self.menu = tk.Menu(self.root)
        self.root.config(menu=self.menu)
        # file menu
        filemenu = tk.Menu(self.menu)
        self.menu.add_cascade(label="File", menu=filemenu)
        filemenu.add_command(
            label="New",
            command=self.canvas.new_file,
            accelerator="CTRL-N"
            )
        filemenu.add_command(
            label="Open",
            command=self.canvas.load_file,
            accelerator="CTRL-O"
            )
        filemenu.add_command(
            label="Save",
            command=self.canvas.save_file,
            accelerator="CTRL+S"
            )
        filemenu.add_command(
            label="Save As...",
            command=self.canvas.save_file_as,
            accelerator="CTRL+ALT+S"
            )
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.root.quit)
        # tools menu
        toolmenu = tk.Menu(self.menu)
        self.menu.add_cascade(
            label="Tools",
            menu=toolmenu,
            )
        toolmenu.add_command(
            label="Load Grid",
            command=self.load_grid,
            )
        toolmenu.add_command(
            label="Save Grid as",
            command=self.save_grid,
            )
        toolmenu.add_command(
            label="Clear Grid",
            command=self.clear_grid,
            )
        # help menu
        helpmenu = tk.Menu(self.menu)
        self.menu.add_cascade(
            label="Help",
            menu=helpmenu,
            )
        helpmenu.add_command(
            label="About",
            command=self.about,
            accelerator="F1")

        self.root.bind("<Control-o>", self.canvas.load_file)
        self.root.bind("<Control-s>", self.canvas.save_file)
        self.root.bind("<Alt-Control-s>", self.canvas.save_file_as)
        self.root.bind("<Control-n>", self.canvas.new_file)
        self.root.bind("<F1>", self.about)
        self.root.bind("<Control-z>", self.canvas.undo)
        self.root.bind("<Control-y>", self.canvas.redo)


    def clear_grid(self):
        """ clears the grid of the canvas
        """
        self.canvas.grid.clear()

    def save_grid(self):
        """ saves grid. alwas a "save as"
        """
        filename = fd.asksaveasfilename(
            confirmoverwrite=True,
            filetypes=self.GRID_FILETYPES,
            defaultextension=self.GRID_FILETYPES
            )
        if filename is not None:
            with open(filename, "w") as fp:
                json.dump(self.canvas.grid, fp, indent=2)

    def load_grid(self):
        """ loads a grid. no error handling yet
        """
        filename = fd.askopenfilename(
            filetypes=self.GRID_FILETYPES,
            defaultextension=self.GRID_FILETYPES
            )
        if filename is not None:
            with open(filename) as fp:
                self.canvas.grid = json.load(fp)
                self.canvas.update()

    def todo(self, *args):
        """
        """
        print("todo")

    def about(self, *args):
        """ Print an about text
        """
        self.root.option_add('*Dialog.msg.font', 'SansSerif 10')
        tk.messagebox.showinfo(
            title="About OLAN",
            message=f"OLED-Animator v {__version__}\n"
                    "(c)2023 by Jens Rapp\n\n"
                    "For further information look at "
                    "README.md",
            )

    def loop(self):
        """ mainloop
        """
        tk.mainloop()


def main():
    MainWindow().loop()


if __name__ == '__main__':
    sys.exit(main())

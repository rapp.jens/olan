##############################################################################
# author       # TecDroiD
# date         # 2023-08-16
# ---------------------------------------------------------------------------
# description  #
#              #
#              #
##############################################################################
import tkinter as tk

import base64

# these are the default images for both states
ON_DEFAULT = base64.b64decode(
    """iVBORw0KGgoAAAANSUhEUgAAADAAAAAaCAYAAADxNd/XAAAACXBIWXMAAAsSAAALEgHS3X78AAAA
    GXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAB+JJREFUWIW1mNtvFdcVxn/7MjM+
    xxwOOWCHmwvYgTjQlkhRS6SARKkiIUEbIqQ8JCrtS6r+A21fqkqV+lJV6j/AU6LkJRJtBFFRQhFV
    1Vya3hIFoqSpKLJpE6c2tuFge2b2XrsPZ2Y8B9uUqOmMlvfxsTzzfWt967K3CiGw1qWUUocPHz4Q
    RdGTWuuDWuttxpgtWuvYGIPWGqXUirX431WfGUKoTESqtbDUe/+JiFz33v8+hPCr11577e1wF5Bq
    rb89/vjjx6Io+plSal8JyBhDFEVYa4njGK11ZWsRUEpRvqO+rgIeEcF7f6ddds798Pz587++JwJP
    PPFES0RecM59M89zvPdorbHWYqwhrLOoRkQyMECj2SAZSKhHw2hTPLmKYg948RoJ0hcBXxHxsORJ
    FsC7HnjnXH19OYqib7300kvdNQmcOHFiJIRwvtvt7vPe02q16GwdotXZwGA8QKIisliYa+RcW3+T
    D4du4toG22liYotWit69UkIhBAIQCEgISBAkBDyCD4WJR7zQnA9s/Ydh9IqmORMqEs65d51zx8+c
    OXN9BYFTp04Nzs/Pv93tdvdGUcSOnTtY31pPCIGNGzfSarWI45g8z+l2u3jvYcDw1y0zXNl5C9tp
    YGxJAihXCucXBCQEhAJ8qIEPHleQcMGTB0GcY/f7EQ+/EUPaI5Hn+Tu3bt167OzZswsAtmSysLDw
    /MLCwt5169axa3QUrRR79uxh//79JEkCUOkyz3Omp6eZmJjg0X9ZdrgNvLlvFrUxQSvdiwCKOoNA
    SWAZvCA46RFwweODJ1c9AlY8eaT5+5c8NzanHHl1kIGbFmvtw9ba08AzVQSefvrpI3NzcxeNMTyw
    +wGSOOHo0aMMDw9jjKl0XIYyy7LKJiYmmJ2d5dNhxx8PdDFJhKpJqYxAoNA9BfhQAhdc4XUXPE48
    efDk4snFFZ8d7WnFsVfWE5YceZ6TpumR55577pIFSNP0FwBjY2MYbTh+/DibNm2qKo5SqqoQWuu+
    SrJt2zYA7I1b7L2muL6PKhfqIahr39fBB48rvJ5LLwJaHBqNVgotCg3c3OR550DGV99qoLXGGPNT
    4DHz0UcfPZKm6Y87nQ6Dg4McOnSIkZERkiQhSRLiOCaKoqrS1MGXa5IkdLtdNtyMmH3AEMURsY5I
    dESsLVHNrDZYrTHaYJTGKN2TnVJF5FiOXk2FAP/uZDx4fZCBzBBCGDl37twZG0I4qZRiaGiIdrvN
    2NgY1lqiKCKO46ree++rSDjnKkKldTodZmZm2H49YWZPz3v1CNSTtpRLrnry0EoXni5l1/sZBIIK
    iAoEFQgm8LfdKY/8uVk68Bs2hPC1RqNBHMfs2rWrD1TZuKIoIs9zRIQ8z1c0MKUUSZKgtWbjlGJx
    b4JRy3ACNfCVxh2aAnRY9nOoTBA0gkEKBxglTGxLOfDu+rLxfd1qrbc2m00ANm/ezFqd+W4jR3lZ
    a2nMBpomqaRBUXl8EPLgyAtv1xNcJCBKkEJSdevdqpcPSjPfdkik0E6jtR6xWutNSZKglKLRaCx3
    yKJkZllWfXbOrWj99dnGGEPIHUmwRLandwAnnkwcCAQFonreXM6BHrgSbP3WZW4UBrDYEAaWFEqp
    rbaUgrW2Al5qPMsyRKQqo3meU44X3vtVySitqeaG/9e1/HixWutPRWSnMYbbt2/TaDQqbYcQcM71
    9YGSRNHa+8iEEJBYkSqP8wEtKyVUNqx6F65GCwQpSm6old56lAEGFlXptI+tMWYyy7KdURQxPT1N
    u92uQiUifWTK6KxGwjlHCIHF+xQLPr2nJM7F1ZqY3DFaSNWtpdZD2vMGlVVRn7TAb7IsOxRCYGZm
    hpGRkSoPnHN9BMrv6iRKy7IMpRQz93sWnNxbGb2j2y6bx4kU1uvOZdS2X2/UMVy0SqmXtdY/mZ2d
    JY5jpqamGB4exnvfNybXk/tOAuXvLobJrSneQ32YqMuhPriV3s/F9ypUSagi5pbJBk/wwp4P4+q9
    aZqeVSEEnn322TdF5NHx8XGiKGJ0dJRms1nNQSWBuoxK0FmWsbi4CMDlh25zbWf62UeJPiI1EmE5
    Kpl4Hrqc8MhbSUng9dOnTx+0AM65Hyilfnf16lXGx8eZnJxky5YtNBqNVQnUo7C0tATA5NACl7fP
    oZz6r8PcChL3MMzdN6X58h8saZqWjvsR9O8HXrDWPjM4OMie8Qex2tBsNmm1Wr36Xt9FeU+aplWF
    ujrU5bfjU4jlfx+nZSWRzhQcfrWJma+k+/yLL774bajtB+bm5r7barX2icjD71++ws7RXVhrmZ6e
    XpEL0NtxpZHw+hemeHfrDRT0uuPnuKEJThh7T7P/jYiQLrHYy7e/iMj3Stx9W8qTJ09ut9a+Yq3d
    b62l3W7T2XE/6zptmiYhCZZulPNJ4zYfbLjBlaFZMiPLnfRz2FIGH2jOCpuvKkavGAZmfL1cv5Om
    6fFz5879c1UCAE899dQ659zzWusnrbUYYyhXYw1pExgwaG2KTXz/UFfOP6j+E4mye3rx/acRlSwd
    OgtEt6RvQ18rGme63e53Ll26tPamvn4dO3bsqNb658aYLxpjqFt9Gv2sRyr1+enO45Q1jlXey/P8
    +xcuXHh1NZxrEigAqCNHjnwljuMnjTEHge3W2i1a62Q18Hc71FrrTKisbLWDrY9rB1u/vHjx4p/u
    drD1H2UGWBG7zMIEAAAAAElFTkSuQmCC"""
)


OFF_DEFAULT = base64.b64decode(
    """iVBORw0KGgoAAAANSUhEUgAAADAAAAAaCAYAAADxNd/XAAAACXBIWXMAAAsSAAALEgHS3X78AAAA
    GXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAACCxJREFUWIW1mNtvXNUVxn/7OmM7
    HpNkYkyJCwkFIkgJqghFJKqiINqqCQGExAsqzT9R8dJH+kD7J1R5AMFLpLSV3ZYWlKatSh9QVEG5
    iAQ5xLFTJ2ni8W3Gc87eZ+8+zDlnzjg2qkp7pKU1lzNnvm+vtb619hYxRra6hBDiyJEj3zbGPC+l
    PCylvFspdZeU0iqlkFIihLjN57/d9JkxxtJCCKXPLcmy7FoIYT7Lsr/GGH/1zjvvvB+/BKTY6run
    n376mDHmNaXUw0opqialLH1hWxEQQlD8R9VXwWdZRpqmeO/x3pcEAUIIH3vvX3n77bd/9x8RePbZ
    Z0dDCG9qrU9orUvQWmu0UmxzERtiSUBsJKFU78H9KPYAFRHIgccYSdKU1vIS7e463mc4AalV5b1C
    iPJ/Yoy/Nsb88PTp02tbEnjuuecmtda/VUp9U2uN1ppmx7H/8hJ7F1YY63iUlEilEEoilALVI1EY
    QoAQ9AJQ0IjECMQIMZKFjC+SNebTDutG0vnG3ST3TBB3jiG2DaF8QHjPUnuNG6srtNttlFIIIT70
    3h8/c+bM/G0EXn755ZE0Tf+mtX7EGEMdyaEPr/LwzC20VAijEFr3wGuFUFXLwUuJkDkBRBU/kR54
    l2V80m5xzXVoPbib1YfuxUkYHh6m2WxSr9fx3pOmKe12G2staysrzM7N4b0H+GB1dfXQ1NRUB0AX
    TEIIp+r1+iPGGBo+8oM/fc6uVhdpLcJopNY9AkYhlC5JSK16wNUmUejjhxgJMXBh5SYLNnD9if2s
    Nhs0mzt47LHHmJiYKHCQZRneezqdDrOzswgheGRsjJkvvqDb7T6qtf4F8FIZgZMnT35HKfVnYwx1
    qXjm958yvrSONKYH3hik0QNEhFb56yICPU9RyGIwAsTIpfYSF1YXubJ/kpWd2zhw4AAHDx4s0mOA
    gHOONE1J05RWq8Xly5eJMTIzM0O32yVJkqOvv/76OQ2gtf6ZtRZjDE++f5nx5S6yZivAe75PKI9G
    QUAPplIZhQqDrnfMrybcum+C1eYoj+bgjTEUYlGoknOuLP4QAo1Ggz179jA7O8vevXuZmZlBKfUq
    cEjNzc0dqNfrr1pr2ZkGDr03g9IaaU1uFlnLvbUoa1DWImsWVSt8DTXgbf4723uGMVzprnEtJsw/
    MMGOXU2OHDmCtZZarUatVsNai9YaKWUpKtUeUVzOOQC63e7k9PT0GV2v148bYzDGsO8f/0QJURas
    0LqSQqZP6rbI9NOqLGzZj0CIgcWljOW7duAEPP744xQqZ4yhiH6MEedcCdx7P9B7Go0Ga2tr7Nq1
    i6WlJWKMz2il1FPFw3bPtUoAUldNl0BLswZRktlQI3k65VrKWtIlWE175zCjoyM0m81S45VSGGOo
    1Wplc/Pel31lo9XrdQBGRkZYXV19SkspJ6WUWASN5Q7C2lISe+rS1/zSdL8GNo+M7imV7BHIYoas
    WZKhGneOj/NVLmst3W6XoaEh2u32pJZSTkgpGe6keQOqmBSlJCJEn1RhG8npPK0KQrqn0tH1RAGj
    GB4eLjtxlmWl4hT9yHtffr5xZoqxPwForRFCfK3sA0W4/x+XkhKr+kpT6LxSqizKvEnhvcc5V96T
    ZVl12CsLOpfdoGOMCzHG+9eHLFQmxRgjMcSy/RMjhAAh9OaZEIhZRsxU7iXRe6KAQCRmgSDTHoE0
    oxYiVgiccyX4QnGyLBt4XXTiYrgrIlIUtpQS5xwhhAUdY5wLIdzvNKyMDTPWTok5UDYDWQCVglCk
    GPTI5/cL5weKWHtPzUcaaWB5ebnUeWAAVPW9c660gkTxuRCCdrtNCGFOe+/PCiGOSimZ//oOGh9f
    JWaS4LMy55F+sBZyeRQRCLEkKrwnbCKjkkg9i+xaTlkZqdFqtdi+fXuZTpt14o0kCiIhBNI0pdPp
    4Jw7q51z00KIn0opubBvggc/mke4jCh6qxyFIFTLI0YIvdWWRYR8htB+cLATg9FpZDC5sMLC3Xcw
    NzfHyMjIgNZXCWyMQpqmOOdIkgQhBNevXy/eT6nz58/fmJqa+h4wmVpNfd3RvLHSnwJ6o2ReB/mm
    pKiPnEgMWZ5ivpde3hOcJ6SekDqCc0ifEda7ZCFwc/sQ3nuGhobKFd9q1asksixjfX2dS5cu4Zx7
    79SpU69pgCRJXgkh/CWEwPvfmqR5bZnxVqey0rEsXllZ8Vgd5pTqj9RbDHPbQuS+i9fpjA1zo7dJ
    odlslk2rKptViU2SpHx/8eJFkiQhTdOf9NK4vx9401r7ktaaho9894+fMb7YvW0K/SrjdFHoK9Hz
    yeF9tO4dR2tNo9GgVqvl2dbvEcXKA/g05dPPPmNxcRHn3BtvvfXWjwYInDhxYnh0dPQ9Y8yjWmtq
    SJ788CoPXbzxP93QxDyaLmRcOXAv8088gLcGIUTRnEoiQgiIkcVbt7jw+ee02228938PIRw+ffr0
    +gABgBdeeGG31vo3WusDxYi7c93z0Mwt9iyscEfHo/J971fZUsZKL/FWc3P/PbT2TdK9cztu2xAi
    Sck6XW6uLDO7+C9arVYhpR8kSXJ8enr6aoH5tk39iy++uM17/4aU8vmCROmVYjjJqCMGNvEDg1eu
    54INJxL580M+IhRqE3OfZRmpFKwaMdCFi9fe+zNra2snz507t/WmvnodO3bs+1LKnyul9m92rPLf
    HqlU55rqeFB02gJ0xT5yzv343Xff/cNmOLckkAMQR48ePWitfV4pdRjYrbW+S0pZ2wz8lx1qbXUm
    VBRs5WBroXKw9cuzZ8+e/7KDrX8DcrLHpbZ77rYAAAAASUVORK5CYII="""
)


class ToggleChoiceButton(tk.Button):
    """ this is a text button toggling through Values
    """

    def __init__(self, root, *args, **kwargs):
        """ initialize.
        You can use the following keyword arguments:
        `values` is a list that can be used to set toggle values default is
        on/off
        `choice` sets the index of the current choice. default is zero
        """
        self._values = kwargs.pop("values", ["on", "off"])
        self.choice = tk.IntVar()
        self.choice.set(kwargs.pop("choice", 0))
        super().__init__(root, *args, **kwargs)
        self.config(command=self._toggle)
        self._show()

    def _toggle(self, *args):
        """ toggle through the values
        """
        val = self.choice.get()
        val = (val + 1) % len(self._values)
        self.choice.set(val)
        self._show()

    def _show(self):
        """ show the current value
        """
        self.config(text=self.get())

    def get(self):
        """ get the current value
        """
        return self._values[self.choice.get()]


class ToggleGroup():
    """ this is a group of Toggle buttons
    it enshures that always only one item of that group is toggled
    """
    def __init__(self, default=None):
        """ initializes whatever
        """
        self.items = []
        self.default = None
        if default is not None:
            self.items.append(default)
            self.default = default

    def toggle(self, item):
        """ deactivate all items but the given `item`
        """
        if item.toggled:
            for x in self.items:
                if x != item:
                    x.set(False)
        else:
            if self.default is not None:
                self.default.set(True)

    def add(self, item):
        self.items.append(item)


class ToggleButton(tk.Button):
    """ This is a simple toggle button widget for tkinter
    it has the additional kwargs
        `toggled` as a preset with False as default
        `on_image` to set a proper ON-state image
        `off_image`to set a proper OFF-state image
    """

    def __init__(self, root, *args, **kwargs):
        """ initialize with kwargs. use as normal button
        """
        self._toggled = tk.BooleanVar()
        self._toggled.set(kwargs.pop("toggled", False))
        self._toggle_group = kwargs.pop("group", None)
        self.set_on_image(kwargs.pop("on_image", ON_DEFAULT))
        self.set_off_image(kwargs.pop("off_image", OFF_DEFAULT))

        super().__init__(root,  *args, **kwargs)
        self.config(borderwidth=0)

        if self._toggle_group is not None:
            self._toggle_group.add(self)

        self.__set_image()
        self.bind("<Button-1>", self.__toggle)

    def __set_image(self):
        """ internal, sets the current image
        """
        if self.toggled:
            self.config(image=self._on_image)
        else:
            self.config(image=self._off_image)

    def set_on_image(self, data=ON_DEFAULT):
        """ set the image for on state.
        Parameter `data` sets the image. If it is not set, the default image
        will be used
        """
        self._on_image = tk.PhotoImage(file=data)

    def set_off_image(self, data=OFF_DEFAULT):
        """ set the image for off state
        Parameter `data` sets the image. If it is not set, the default image
        will be used
        """
        self._off_image = tk.PhotoImage(file=data)

    def __toggle(self, *args):
        """ toggles the button if it is enabled
        """
        if self["state"] == "active":
            self._toggled.set(not self._toggled.get())
            self.__set_image()

            if self.toggled and self._toggle_group is not None:
                self._toggle_group.toggle(self)

    def set(self, status):
        """ set the current button value
        """
        self._toggled.set(status)
        self.__set_image()
        if self.toggled and self._toggle_group is not None:
            self._toggle_group.toggle(self)

    @property
    def toggled(self):
        """ get the currenttoggle state
        """
        return self._toggled.get()


class ImageButton(tk.Button):
    """ this is an image button
    """
    def __init__(self, root, image, **kwargs):
        self.image = tk.PhotoImage(file=image)
        super().__init__(
            root,
            image=self.image,
            borderwidth=0,
            **kwargs
            )

##############################################################################
# author       # TecDroiD
# date         # 2023-08-16
# ---------------------------------------------------------------------------
# description  #
#              #
#              #
##############################################################################
import tkinter as tk

import math

from .widgets import ToggleButton, ToggleGroup


class EventHandler():
    """ This is a basic Eventhandler.
    It encapsulates all neccessary base
    events for a drawing program
    """
    def __init__(self, item, config={}):
        """ initialize the handler.
        item is an instance of the AnimationCanvas
        """
        self.item = item
        self.config = config

    def getxy(self, event):
        """ calculate the real coordinates of an event
        """
        return self.item.calc_pos(event.x, event.y)

    def on_button_1(self, event):
        """ this is the handler function called on button 1 click
        """

    def on_button_2(self, event):
        """ this is the handler function called on button 2 click
        """

    def on_button_3(self, event):
        """ this is the handler function called on button 2 click
        """

    def on_motion(self, event):
        """ when the mouse is moved, this function should be called
        """

    def on_key_press(self, event):
        """ this is called when a key has been pressed
        """


class DotPainter(EventHandler):
    """ just draw pixels
    right click erases
    """

    def on_motion(self, event):
        """ on motion create a pixel
        """
        x, y = self.getxy(event)
        snapshot = self.item.snapshot()
        if event.state & 0x100:
            self.item.setpixel(x, y, 1)
        elif event.state & 0x400:
            self.item.setpixel(x, y, 0)
        self.item.done(snapshot)


class MultiClickHandler(EventHandler):
    """ this is a handler which creates a rectangle
    around the current destination
    """
    def __init__(self, item, config, max_state=2):
        """ initialize the Painter
        """
        super().__init__(item, config)
        self.state = 0
        self.max_state = max_state
        self.px = 0
        self.py = 0
        self.frameid = None
        self.snapshot = None

    def _draw_frame(self, x1, y1, x2, y2):
        """ draw a frame for the current item
        """
        return self.item.create_rectangle(
            x1 * self.item.scale, y1 * self.item.scale,
            x2 * self.item.scale, y2 * self.item.scale,
            )

    def getxy(self, event):
        """ calculate the real coordinates of an event
        If CTRL is pressed after the first click, it
        normalizes to 45 degree angles using delta x as distance on diagonals
        TODO: If SHIFT is pressed after the first click, it uses those
        coordinates as center instead of the first corner
        """
        x, y = super().getxy(event)

        if self.state > 0 and event.state & 0x0004:
            delta_x = x - self.px
            if delta_x == 0:
                delta_x = 0.01
            delta_y = y - self.py
            if delta_y == 0:
                delta_y = 0.01

            if delta_x == 0 or abs(delta_x / delta_y) < 0.7:
                x = self.px
                y = self.py + delta_y
            elif delta_y == 0 or abs(delta_y / delta_x) < 0.7:
                x = self.px + delta_x
                y = self.py
            else:
                x = self.px + delta_x
                if delta_x > 0:
                    if delta_y > 0:
                        y = self.py + delta_x
                    else:
                        y = self.py - delta_x
                else:
                    if delta_y > 0:
                        y = self.py - delta_x
                    else:
                        y = self.py + delta_x

        return x, y

    def reset(self, done=True):
        """ reset the items and put the snapshot onto the undostack
        """
        if done and self.snapshot is not None:
            self.item.done(self.snapshot)
            self.snapshot = None
        if self.frameid is not None:
            self.item.delete(self.frameid)
            self.state = 0

    def on_button_1(self, event):
        """ left button event
        """
        if self.state == 0:
            self.px, self.py = self.getxy(event)
            self.snapshot = self.item.snapshot()
        self.state += 1

    def on_button_3(self, event):
        """ right button event
        """
        self.state = 0
        self.reset(False)

    def on_motion(self, event):
        """ scetch a rectangle around the destination
        """
        # if a frame is drawn, delete the frame
        if self.frameid is not None:
            self.item.delete(self.frameid)

        # get real x y coordinates
        x, y = self.getxy(event)

        # after first click draw a frame
        if self.state != 0:
            self.frameid = self._draw_frame(
                self.px, self.py,
                x, y
                )


class LinePainter(MultiClickHandler):

    def __init__(self, item, config):
        super().__init__(item, config=config)

    def __line_helper(self, da, db, a, b, drawfunc):
        """ draw a line which is agnostic concerning x or y position
        """
        step = db / da
        pb = b
        for pos in range(da):
            pb += step
            drawfunc(a + pos, int(pb))

    def calc_line(self, dx, dy, color=1):
        """ how do i tell you this? well.. let's just draw a line
        """

        # first, we get our points and calculate the needed pixels in x and y
        xa = self.px
        ya = self.py
        xb = dx
        yb = dy

        xab = xb - xa
        yab = yb - ya

        if abs(xab) > abs(yab):
            # if our x axis is longer than y, we'll calculate with this
            if xab < 0:
                self.__line_helper(
                    -xab, -yab, xb, yb,
                    lambda x, y: self.item.setpixel(x, y, color)
                    )
            else:
                self.__line_helper(
                    xab, yab, xa, ya,
                    lambda x, y: self.item.setpixel(x, y, color)
                    )
        else:
            # otherwise we'll calculate on y
            if yab < 0:
                self.__line_helper(
                    -yab, -xab, yb, xb,
                    lambda y, x: self.item.setpixel(x, y, color)
                    )
            else:
                self.__line_helper(
                    yab, xab, ya, xa,
                    lambda y, x: self.item.setpixel(x, y, color)
                    )

    def _draw_frame(self, x1, y1, x2, y2):
        """ draw a frame for the current item
        """
        return self.item.create_line(
            x1 * self.item.scale, y1 * self.item.scale,
            x2 * self.item.scale, y2 * self.item.scale
            )

    def on_button_1(self, event):
        """ on button 1 set
        """
        super().on_button_1(event)
        x, y = self.getxy(event)
        # shift removes the content
        if event.state & 0x0001:
            color = 0
        else:
            color = 1

        if self.state == 2:
            self.calc_line(x, y, color)
            self.reset()


class SupportLinePainter(MultiClickHandler):

    def __init__(self, item, config):
        super().__init__(item, config=config)

    def _draw_frame(self, x1, y1, x2, y2):
        return self.item.create_line(
            x1 * self.item.scale, y1 * self.item.scale,
            x2 * self.item.scale, y2 * self.item.scale
            )

    def on_button_1(self, event):
        """ on button 1 set
        """
        super().on_button_1(event)
        x, y = self.getxy(event)
        if self.state == 2:
            self.item.create_gridline(
                self.px, self.py, x, y
                )
            self.reset()


class RectanglePainter(MultiClickHandler):
    """ This draws a rectangle
    """

    def __init__(self, item, config):
        super().__init__(item, config=config)
        self.px = 0
        self.py = 0

    def _hor_line(self, y, start, end, color=1):
        """ draw horizontal lines
        """
        for x in range(start, end+1):
            self.item.setpixel(x, y, color)

    def _vert_line(self, x, start, end, color=1):
        """ draw a vertical line
        """
        for y in range(start, end+1):
            self.item.setpixel(x, y, color)

    def calc_lines(self, dx, dy, color=1):
        """ calculate the rectangle
        """
        xa = self.px
        ya = self.py
        xb = dx
        yb = dy
        if ya > yb:
            xa = dx
            ya = dy
            xb = self.px
            yb = self.py

        if self.config.get("filled", False):
            # first, draw horizontal lines
            for y in range(ya, yb + 1):
                # todo: there will be one line missing
                if xa > xb:
                    self._hor_line(y, xb, xa, color)
                else:
                    self._hor_line(y, xa, xb, color)
        else:
            # first, draw horizontal lines
            if xa > xb:
                self._hor_line(ya, xb, xa, color)
                self._hor_line(yb, xb, xa, color)
            else:
                self._hor_line(ya, xa, xb, color)
                self._hor_line(yb, xa, xb, color)

            # and then vertical ones
            self._vert_line(xa, ya, yb, color)
            self._vert_line(xb, ya, yb, color)

    def on_button_1(self, event):
        super().on_button_1(event)
        x, y = self.getxy(event)

        # shift removes the content
        if event.state & 0x0001:
            color = 0
        else:
            color = 1

        if self.state == 2:
            self.calc_lines(x, y, color)
            self.reset()


class CirclePainter(MultiClickHandler):
    """ draws a circle
    """

    def __init__(self, item, config):
        super().__init__(item, config=config)
        self.px = 0
        self.py = 0

    def _draw_frame(self, x1, y1, x2, y2):
        """ draw a frame for the current item
        """
        return self.item.create_oval(
            x1 * self.item.scale, y1 * self.item.scale,
            x2 * self.item.scale, y2 * self.item.scale,
            )
    def _hor_line(self, y, start, end, color):
        """ draw horizontal lines
        """
        for x in range(start, end+1):
            self.item.setpixel(x, y, color)

    def _vert_line(self, x, start, end, color):
        """ draw a vertical line
        """
        for y in range(start, end+1):
            self.item.setpixel(x, y, color)

    def calc_ellipse(self, x, y, color):
        """ draw the ellipse
        """
        dx = abs(int((self.px - x) / 2))
        dy = abs(int((self.py - y) / 2))

        # calculate pixels
        num = dx if dx > dy else dy
        num = int(num * math.pi)

        centerx = self.px + dx if self.px < x else x + dx
        centery = self.py + dy if self.py < y else y + dy

        for i in range(num):
            angle = (math.pi * i)/num
            px = int(dx * math.cos(angle))
            py = int(dy * math.sin(angle))
            if self.config["filled"] is True:
                self._vert_line(centerx + px, centery - py, centery + py, color)
                #self._hor_line(centery + py, centerx - px, centerx + px, color)
            else:
                self.item.setpixel(centerx + px, centery + py, color)
                self.item.setpixel(centerx - px, centery - py, color)

    def on_button_1(self, event):
        """ draw the ellipse
        """
        super().on_button_1(event)
        x, y = self.getxy(event)

        # shift removes the content
        if event.state & 0x0001:
            color = 0
        else:
            color = 1

        if self.state == 2:
            self.calc_ellipse(x, y, color)
            self.reset()


class ToolBox(tk.Frame, EventHandler):
    """ this is a drawing toolbox
    """
    def __init__(self, root, item, **kwargs):
        """ initialize using the animation canvas as parameter
        """
        tk.Frame.__init__(self, root, relief=tk.SUNKEN, **kwargs)
        EventHandler.__init__(self, item)
        self.config["filled"] = False

        self.dots = DotPainter(self.item, self.config)
        self.lines = LinePainter(self.item, self.config)
        self.rectangles = RectanglePainter(self.item, self.config)
        self.circles = CirclePainter(self.item, self.config)
        self.supports = SupportLinePainter(self.item, self.config)

        row=0
        self._set_active(self.dots)
        self.group = ToggleGroup()

        tk.Label(self, text="Draw").grid(column=0, columnspan=2, row=row)
        row += 1

        dots = ToggleButton(
            self,
            off_image="grfx/dots.png",
            on_image="grfx/dots_active.png",
            toggled=True,
            group=self.group,
            command=lambda: self._set_active(self.dots)
            )
        dots.grid(column=0, row=row)

        self.group.default = dots

        ToggleButton(
            self,
            off_image="grfx/line.png",
            on_image="grfx/line_active.png",
            group=self.group,
            command=lambda: self._set_active(self.lines)
            ).grid(column=1, row=row)

        row += 1

        ToggleButton(
            self,
            off_image="grfx/circle.png",
            on_image="grfx/circle_active.png",
            group=self.group,
            command=lambda: self._set_active(self.circles)
            ).grid(column=0, row=row)

        ToggleButton(
            self,
            off_image="grfx/rectangle.png",
            on_image="grfx/rectangle_active.png",
            group=self.group,
            command=lambda: self._set_active(self.rectangles)
            ).grid(column=1, row=row)

        row += 1
        tk.Label(self, text="Tool").grid(column=0, columnspan=2, row=row)
        row += 1

        self.fill = ToggleButton(
            self,
            off_image="grfx/fill.png",
            on_image="grfx/fill_active.png",
            )
        self.fill.grid(column=0, row=row)
        self.fill.config(command=self.toggle_fill)

        row += 1
        tk.Label(self, text="Grid").grid(column=0, columnspan=2, row=row)
        row += 1

        ToggleButton(
            self,
            off_image="grfx/supportline.png",
            on_image="grfx/supportline_active.png",
            group=self.group,
            command=lambda: self._set_active(self.supports),
            ).grid(column=0, row=row)

        self.bind_events()
        self.pack(side=tk.LEFT, fill=tk.Y)


    def toggle_fill(self):
        """ toggle filled elements
        """
        self.config["filled"] = self.fill.toggled

    def bind_events(self):
        """ this binds all of our eventhandler functions to the item
        """
        self.item.bind("<Button-1>", self.on_button_1)
        self.item.bind("<Button-2>", self.on_button_2)
        self.item.bind("<Button-3>", self.on_button_3)
        self.item.bind("<Escape>", self.on_button_3)
        self.item.bind("<Motion>", self.on_motion)
        self.item.bind("<Key>", self.on_key_press)

    def _set_active(self, handler):
        """ set the active handler
        """
        self.handler = handler

    def on_button_1(self, event):
        """ this is the handler function called on button 1 click
        """
        self.handler.on_button_1(event)

    def on_button_2(self, event):
        """ this is the handler function called on button 2 click
        """
        self.handler.on_button_2(event)

    def on_button_3(self, event):
        """ this is the handler function called on button 2 click
        """
        self.handler.on_button_3(event)

    def on_motion(self, event):
        """ when the mouse is moved, this function should be called
        """
        self.item.moved(event)
        self.handler.on_motion(event)

    def on_key_press(self, event):
        """ this is called when a key has been pressed
        """
        self.handler.on_key_press(event)

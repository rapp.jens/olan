#!/usr/bin/bash

if [ $#  -ne 1 ]; then
	echo "usage:"
	echo "$0 repository (pypi,testpypi,...)"
	exit 1
fi

repository=$1

python3 -m twine upload --repository $repository dist/*

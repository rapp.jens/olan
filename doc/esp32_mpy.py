##############################################################################
# author       # TecDroiD
# date         # 2023-08-18
# ---------------------------------------------------------------------------
# description  # TODO: die Scheiß Frames sind base64 encoded
#              #
#              #
##############################################################################
from machine import I2C
import time
import framebuf
import json
import binascii

import sh1106


def create_display():
    """ create the display connection
    """
    i2c = I2C(0, freq=400000)
    print(i2c.scan())  # display should be ID 60
    display = sh1106.SH1106_I2C(128, 64, i2c)
    display.flip()
    return display


def load_animation(filename):
    """ load the file
    """
    with open(filename) as fp:
        vals = json.load(fp)
        for i, frame in enumerate(vals["frames"]):
            vals["frames"][i] = bytearray(binascii.a2b_base64(frame))
    return vals


def get_frame(animation, frame):
    fno = frame % len(animation["frames"])
    buf = framebuf.FrameBuffer(
        animation["frames"][fno],
        128, 64,
        framebuf.MONO_HLSB
        )
    return buf


if __name__ == '__main__':
    # now create the display
    display = create_display()
    # load the animation
    animation = load_animation("normal.ani")
    # and set the current image
    current = 0

    while True:
        buf = get_frame(animation, current)
        display.blit(buf, 0, 0)
        display.show()
        current += 1
        if current >= len(animation["frames"]):
            time.sleep(5)
            current = 0
        time.sleep(1/10)

from machine import I2C
import time
import sh1106

i2c = I2C(0, freq=100000)
i2c.scan()
display = sh1106.SH1106_I2C(128, 64, i2c)

display.init_display()
time.sleep(0.2)

display.fill(0x00)
display.ellipse(32, 32, 10, 15, 255)
display.ellipse(32, 42, 4, 5, 255, True)

display.ellipse(96, 32, 10, 15, 255)
display.ellipse(96, 42, 4, 5, 255, True)

display.show()

# OLed ANimator - manual
This manual will describe how to create animations and deploy them to a
microcontroller.

# Install and run
Just clone it and run it..

    $ git clone https://gitlab.com/rapp.jens/olan.git
    $ cd olan
    $ ./oled_animator

You should be able to see the ui now.

# The UI
After running the program you can see a really simple ui.

![the really simple ui](grfx/olan-screen.png "OLAN-GUI")

## Menu
The menu consists of the file menu to load or save files. feel free to make an
educated guess how this works.

Next is the tools menu which enables you to load and save grid schemes.
It's also possible to delete the current grid. These grids are thin lines which
won't be drawn into your animation. They don't even appear in an animation.
But you can always load one onto your canvas. Grids can help you to segmentize
your canvas for easier orientation and higher precision drawing.

## Canvas
Biggest part of the window is the canvas which will enable you to edit animation
frames.

When you move the mouse, a green cross will follow you to help drawing more
precise.

## Toolbar
At the left side, you can  see the tool bar which consists of the four drawing
tools for dots (free hand tool), lines, ovals and rectangles. By default,
ovals and rectangles are empty.

### Drawing free hand
Drawing free hand is enabled by default. You can use this tool by left or right
clicking on the canvas and moving the mouse.
Left click will enable pixels, whereas right click removes them. You can use
this tool as eraser.

### Drawing lines
Drawing a line can be done by enabling the line tool on the toolbar. Position
the mouse to the coordinates where the line should start and click left. When
you move the mouse you will see a thin line which shows where the drawn line
will be. Position the mouse to the end of your desired line and click left
again. Your line will be drawn- magic!

Your can abort this by clicking the right mouse button.

Pressing the CTRL key enables an 45 degrees alignment to enable you to draw
horizontal, vertical or diagonal lines.

Pressing the SHIFT key while doing the second click will result in an inverted
line. You can use this to delete parts of the image.

### Drawing rectangles and Ovals
Rectangles and Ovals work basically the same way as a line does.
Click left for the first corner, move and click left again for the other corner.
While dragging, you will see a rough shape of what will be drawn.

A right click will also abort while holding CTRL results in a square or circle.

You can enable filled shapes by activating the fill
tool button in the section "Tool". Flood fill is currently not possible..

Pressing the SHIFT key while doing the second click will result in an inverted
shape. You can use this to delete parts of the image.

### Drawing a grid

Last part of that bar is the "Grid" tool which enables you to draw thin lines
which won't be visible in your animation but help you to segmentize
the screen according to your needs.

Drawing grid lines works similar to normal lines.

Grids can be loaded and saved. This enables you to use them for different
animations. This function is originally used for orientation when i draw faces.

For loading a grid, got to the Tools menu and click "Load grid". This leads you
to a file menu which I believe you can handle.

Grid files are basically json files. They consist of an array which contains
line coordinates.

For example, a cross through the center will look this way:

    [
        [0, 31, 127, 31],
        [63, 0, 63, 63]
    ]

This means, it has two lines. A horizontal one which goes from pixel 0 to 127
at line 31 and a vertical one from 0 to 63 at column 63 which should be pretty
much the center.
If clicking the grids via my UI isn't your thing, just go ahead and write your
own grids in a text editor.

## Animation bar

At the bottom, you can find the animation bar which enables you to navigate
between images. The following functions are given (left to right):

- first: go to the first image
- previous: go to the previous image
- number: choose a direct number to go to that image
- next: go to the next image
- last: go to the last image
- Run: not working yet but once I find the time to implement, it previews the
animation
- Add: add a new image directly after the current
- Delete: remove the current image (cannot be undone)
- Copy: copy the current image directly after the current one

When you navigate between the images you will always see the previous image
content in light red. Content of the next image will be displayed in a light
blue.

![previous and next image](grfx/draw_ball.png "See the next and previous image")

# Tips for Creating an Animation

## creating a bouncing animation (eg. Jumping ball)
To create an animation which loops forward and backward like a bouncing ball or
eyes which look from left to right and back, you can follow these steps:

- draw the first image
- hit "copy image" button
- navigate one image back (first one of the two copies)
- hit "add image" button. you will see an empty frame which also shows both,
the previous and next image. Since both are identical you should only see light
blue dots.


# Using the Animation with micropython
The following example uses an ESP32 microcontroller equipped with micropython.
I assume that the display is correctly connected to the ESP.

    DISPLAY             ESP32
    VCC                 +3V3
    GND                 GND
    SCL                 GPIO22
    SDA                 GPIO21

I also assume that the display utilizes the sh1106 display controller.
Same should also work on a ssd1306 (0.96") but you'd have to exchange the
display driver python module (sh1106.py).
This display driver is basically a framebuf which can blit itself into a display
via I2C or SPI.

Now lets begin. We're opening the display, load the animation file and create a
simple endless animation loop.

    from machine import I2C
    import time
    import framebuf
    import json
    import binascii

    import sh1106


    def create_display():
        """ create the display connection
        """
        i2c = I2C(0, freq=400000)
        print(i2c.scan())  # display should be ID 60
        display = sh1106.SH1106_I2C(128, 64, i2c)
        display.flip()
        return display


    def load_animation(filename):
        """ load the file
        """
        with open(filename) as fp:
            vals = json.load(fp)
            for i, frame in enumerate(vals["frames"]):
                vals["frames"][i] = bytearray(binascii.a2b_base64(frame))
        return vals


    def get_frame(animation, frame):
        """ read a single frame from the buffer
        """
        fno = frame % len(animation["frames"])
        buf = framebuf.FrameBuffer(
            animation["frames"][fno],
            128, 64,
            framebuf.MONO_HLSB
            )
        return buf


    if __name__ == '__main__':
        # now create the display
        display = create_display()
        # load the animation
        animation = load_animation("sceptic.ani")
        # and set the current image
        current = 0

        while True:
            # blit the frame
            buf = get_frame(animation, current)
            display.blit(buf, 0, 0)
            display.show()
            # increase the current frame
            current += 1
            current = current % len(animation["frames"])
            # and wait a sec.. okay only a tenth sec..
            time.sleep(1/10)

Upload both, _sceptic.ani_ and _esp32_mpy.py_ to the microcontroller

    $ ampy -p /dev/ttyACM0 put sceptic.ani
    $ ampy -p /dev/ttyACM0 put esp32_mpy.py main.py

now restart your microcontroller and sceptic eyes should make you happy
